<div class="holder">
  <div class="header">      
    <div id="menu">
      <?php print render($page['header']); ?>
    </div>
  
    <?php if ($logo): ?>
      <div class="logo">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      </div>
    <?php endif; ?>

    <br clear="all" />

    <?php if ($main_menu): ?>
      <div id="main-menu" class="sublinks">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
          )
        )); ?>
      </div> <!-- /#main-menu -->
      <br clear="all" />
    <?php endif; ?>

  </div>

  <?php print render($page['banner']); ?>

  <div class="leftsides">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="title" id="page-title">
        <?php print $title; ?>
      </h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
    
    <?php if ($page['content']): ?>
    <div class="welcome">
      <?php print render($page['content']); ?>
      <br clear="all" />
    </div>
    <?php endif; ?>

    <br clear="all" />
    
    <?php if ($page['featured']): ?>
      <div class="lower_holder">
        <?php print render($page['featured']); ?>
        <br clear="all" />
      </div>
    <?php endif; ?>
  </div>

  <?php if ($page['sidebar_second']): ?>
    <div class="rightside">  
      <?php print render($page['sidebar_second']); ?>
    </div>
  <?php endif; ?>
  <br clear="all" />

  <?php print render($page['footer']); ?>
</div>