<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
//print "<pre>"; print_r($rows); die;
?>
<div class="leftmid">
	<div id="small_slider">
	    <?php 
	    foreach ($rows as $row_count => $row):
	    	print theme('image_style', array('style_name' => 'style_250_188', 'path' => $row['uri'], 'attributes' => array('title' => '#htmlcaption'.$row_count)));	
		endforeach; 
		?>
	</div>
	<?php foreach ($rows as $row_count => $row): ?>
		<div id="htmlcaption<?php print $row_count ?>" class="nivo-html-caption">
	        <?php print $row['title']; ?>
	    </div>
	<?php endforeach; ?>
</div>