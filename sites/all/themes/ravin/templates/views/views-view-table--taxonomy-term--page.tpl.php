<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

//print "<pre>"; print_r($rows); die;
?>
<div class="media">
	<h3><?php echo $title; ?></h3>
    <?php 
    foreach ($rows as $row_count => $row): ?>
    
    <div class="article_ad">
    	<p class="firstrow">
    		<?php print $row['colorbox']; ?>
    		<?php print $row['field_ad_image']; ?>
    		<h4><?php print $row['title']; ?></h4>
    	</p>
    </div>
	<?php endforeach; ?>
</div>