<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
/*echo '<pre>';
print_r($rows);
exit;
*/

$open_tag = '';
$close_tag = '';
if($title=="Regional Office") {
	$open_tag = '<div class="address">';
	$close_tag = "</div>";
}

?>
<div class="page-title"><?php echo $title; ?></div>

	<?php 
	foreach ($rows as $row_count => $row): print $open_tag; ?>
	<ul class="contactoffice">
	<?php
		print '<li><h4>'.$row['title'].'</h4></li>';
		print !empty($row['field_address']) ? '<li><b>'.t("Address").': </b>'.$row['field_address'].'</li>' : '';
		print !empty($row['field_telephone']) ? '<li><b>'.t("Tel").': </b>'.$row['field_telephone'].'</li>' : '';
		print !empty($row['field_fax']) ? '<li><b>'.t("Fax").': </b>'.$row['field_fax'].'</li>' : '';
		print !empty($row['field_sales']) ? '<li><b>'.t("Sales").': </b>'.$row['field_sales'].'</li>' : '';
		print !empty($row['field_purchase']) ? '<li><b>'.t("Purchase").': </b>'.$row['field_purchase'].'</li>' : '';
		print !empty($row['field_corporate_communication']) ? '<li><b>'.t("Corporate Communication").': </b>'.$row['field_corporate_communication'].'</li>' : '';
		print !empty($row['field_feedback']) ? '<li><b>'.t("Feedback").': </b>'.$row['field_feedback'].'</li>' : '';
		print !empty($row['field_customer_care']) ? '<li><b>'.t("Customer care").': </b>'.$row['field_customer_care'].'</li>' : '';
		print !empty($row['field_human_resources']) ? '<li><b>'.t("Human Resources").': </b>'.$row['field_human_resources'].'</li>' : '';
	?>
	</ul>
	<?php echo $close_tag;
	 endforeach; 
	?>

<br clear="all">