<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

//print "<pre>"; print_r($rows); die;
?>
			<!-- <a href="images/media/Our_CMD_Mr_Vijay_Karia_with_Shri_Devendra_Fadnavis_CM_of_Maharashtra.jpg" title="Our CMD Mr.Vijay Karia with Shri Devendra Fadnavis CM, of Maharashtra" rel="8" >
				<img src="images/media/Our_CMD_Mr_Vijay_Karia_with_Shri_Devendra_Fadnavis_CM_of_Maharashtra.jpg" border="1" width="145" height="95" />
	    		<div class="desc">Our CMD Mr.Vijay Karia with Shri Devendra Fadnavis CM, of Maharashtra</div>
	  		</a> -->
<div class="media">
	<h3><?php echo $title; ?></h3>
    <?php 
    foreach ($rows as $row_count => $row): ?>
	    <div class="event_type">
	    	<div class="heading_img">
	    		<div class="label-title"><?php print $row['title']; ?></div>
	    	</div>
	    	<a href="javascript:void(0);" class="hannover_banner" data-id="<?php print $row_count; ?>"><?php print $row['field_image']; ?></a>
	    </div>
	    <div class="media hannover_images" id="hannover_images<?php print $row_count; ?>">
	    <?php 
	    $other_images = explode(",", $row['field_image_1']); 
	    foreach($other_images as $other_image): print $other_image; endforeach; 
	    ?>
		</div>
	<?php endforeach; ?>
	
</div>