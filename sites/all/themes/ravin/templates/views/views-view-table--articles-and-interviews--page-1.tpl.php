<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

//print "<pre>"; print_r($rows); die;
?>
<div class="media">
	<h3><?php echo $title; ?></h3>
    <?php 
    foreach ($rows as $row_count => $row): ?>
    
    <div class="article">
    	<div class="label-title"><?php print $row['title']; ?></div>
    	<p class="firstrow"><?php print $row['field_image']; ?></p>
    </div>
	<?php endforeach; ?>
</div>