jQuery(document).ready(function() {
	jQuery('#slider').nivoSlider({
		effect:'fade'
	});

	jQuery('#small_slider').nivoSlider({
		effect:'fade', 
		height:'175px'
	});

	jQuery('#client').nivoSlider({
		effect:'fade',
		//height:'175px'
	});

	jQuery('.hannover_images').hide();
      
    jQuery('.hannover_banner').click(function(){
    	var data_id = jQuery(this).attr('data-id');
		jQuery('#hannover_images'+data_id).toggle();
    });

    jQuery('.st-accordion').accordion({
        oneOpenedItem: true,
    });

    jQuery('.webform-component--tel-no #edit-submitted-tel-no').before(jQuery('.webform-component--tel-no-prefix').html());
    jQuery('.webform-component--tel-no-prefix').html('');

    jQuery('.webform-component--mobile-no #edit-submitted-mobile-no').before(jQuery('.webform-component--mobile-no-prefix').html());
    jQuery('.webform-component--mobile-no-prefix').html('');

});